#!/usr/bin/python
# -*- coding: utf-8 -*-


# Clase que representa a una tarjeta
class Tarjeta():

    # Constructor de la clase tarjeta
    def __init__(self, nombre, saldo):
        # Los objetos son privados
        self.__nombre = nombre
        self.__saldo = saldo

    def get_nombre(self):
        return self.__nombre

    def get_saldo(self):
        return self.__saldo

    def set_nombre(self, nombre):
        self.__nombre = nombre

    def set_saldo(self, saldo):
        try:
            s = int(input("Ingrese el valor de la compra: "))
            # Si el saldo es mayor a 0 entonces se puede realizar una compra
            if s < saldo > 0:
                self.__saldo -= s
                print("Ha hecho una compra, su nuevo saldo es: ", self.__saldo)
            else:
                print("No tiene suficiente dinero para realizar la compra")

        except ValueError:
            print("No ingreso una cifra")


# Función principal o main
if __name__ == '__main__':
    Usuario = Tarjeta("Josefa", 50000)
    Usuario.set_nombre("Nombre del titular: Josefa Hillmer")
    # Para acceder al objeto privado
    print(Usuario.get_nombre())
    print("Saldo de la tarjeta: ", Usuario.get_saldo())
    # Para modificar el objeto privado
    Usuario.set_saldo(50000)
