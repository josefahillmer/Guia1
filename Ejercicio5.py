#!/usr/bin/python
# -*- coding: utf-8 -*-


# Clase que representa a un Perro
class Perro():
    # Constructor de la clase Perro
    def __init__(self, color, raza, edad, sexo, peso):

        print("El perro ha sido creado")
        self.color = color
        print("Color: ", color)
        self.raza = raza
        print("Raza: ", raza)
        self.edad = edad
        print("Edad: ", edad, "años")
        self.sexo = sexo
        print("Sexo: ", sexo)
        self.peso = peso
        print("Peso :", peso, "Kg")


class Acciones(Perro):

    def jugar(self):
        # print el perro juega
        print("El perro juega")

    def ladrar(self):
        # El perro ladra
        print("El perro ladra: Guauu!")

    def dormir(self):
        # El perro duerme
        print("El perro duerme zzzz")

    def comer(self):
        # El perro come
        print("El perro come")

    def agua(self):
        # El perro toma agua
        print("El perro toma agua")


# Funcion principal o main
if __name__ == '__main__':
    Mascota = Perro("Negro con blanco", "Border collie", 3, "Hembra", 20)
    Acciones = Acciones()
    Acciones.jugar()
    Acciones.ladrar()
    Acciones.dormir()
    Acciones.comer()
    Acciones.agua()

