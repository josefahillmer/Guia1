#!/usr/bin/python
# -*- coding: utf-8 -*-


# Clase que representa a una cuenta
class Cuenta():

    # Constructor de la clase tarjeta
    def __init__(self, nombre, cuenta, saldo):
        # Los objetos son privados
        self.__nombre = nombre
        self.__cuenta = cuenta
        self.__saldo = saldo

    def get_nombre(self):
        return self.__nombre

    def get_cuenta(self):
        return self.__cuenta

    def get_saldo(self):
        return self.__saldo

    # Metodo de ingreso
    def ingreso(self):
        ingreso = float(input('Dinero de ingreso: '))
        if ingreso > 0:
            self.__saldo += ingreso
            print("Se a realizado un deposito")
            print("Su nuevo saldo es: ", self.__saldo)
        else:
            print("Deposito fallado")

    # Metodo de giro
    def giro(self):
        giro = float(input('Dinero a girar: '))
        if giro > 0 and self.__saldo >= giro:
            self.__saldo -= giro
            print("Se a realizada un giro")
            print("Su nuevo saldo es: ", self.__saldo)
        else:
            print("Giro fallado")

    # Metodo de transferencia
    def trans(self):
        saldo = float(input('Dinero a transferir: '))
        # Debe ser mayor a 0 y que el saldo final no sea negativo
        if saldo > 0 and self.__saldo >= saldo:
            print("Ingrese datos de cuenta a transferir:")
            cuenta2 = int(input("Ingrese numero de cuenta: "))
            self.__saldo -= saldo
            print("Se ha realizado una transferencia a", cuenta2)
            print("Su nuevo saldo es: ", self.__saldo)
        else:
            print("Transferecia fallada")


# Función principal o main
if __name__ == '__main__':

    try:
        nombre = "Josefa Hillmer"
        cuenta = 9000236521
        saldo = 100000
        Usuario = Cuenta(nombre, cuenta, saldo)
        print('1. Ver datos\n2. Ingreso de dinero')
        print('3. Giro de dinero\n4. Transferencia a otra cuenta')
        # Usuario ingresa una opcion
        opcion = int(input('Por favor, seleccione una opción: '))

        if opcion == 1:
            print("Nombre del cliente: ", Usuario.get_nombre())
            print("Numero de cuenta: ", Usuario.get_cuenta())
            print("Saldo: $", Usuario.get_saldo())

        if opcion == 2:
            deposito = Usuario.ingreso()

        if opcion == 3:
            giros = Usuario.giro()

        if opcion == 4:
            transferir = Usuario.trans()

    except ValueError:
        print("Opcion no valida")

