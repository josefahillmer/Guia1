#!/usr/bin/python
# -*- coding: utf-8 -*-


class Algoritmo():

    def __init__(self, numero):
        self.numero = numero

    def suma(self, numero):
        lista = []
        # Se crea una lista con los digitos del numero
        for i in str(numero):
            lista.append(int(i))
        self.lista = lista
        Algoritmo.numero_grandes(self.lista)

    def numero_grandes(self, lista):

        lista2 = []
        suma = 0
        # Se recorre la lista para sacar solo el ultimo digito
        # de un numero mayor a 10
        for i in range(len(lista)):
            suma = int((suma + lista[i]) % 10)
            lista2.append(suma)
        self.lista2 = lista2
        Algoritmo.suma_7(self.lista2)

    def suma_7(self, lista2):

        lista3 = []
        # Se recorre la lista y se le suma 7
        for i in range(len(lista2)):
            lista3.append((7 + lista2[i]) % 10)
        self.lista3 = lista3
        Algoritmo.intercambio(self.lista3)

    def intercambio(self, lista3):
        lista4 = 0
        # Se recorre la lista para invertirla y correrla 3 espacios
        for i in range(len(lista3)):
            lista4 += (lista3[3 - i]) * (10 ** i)
        self.__intercambio = lista4

    def get_intercambio(self):
        return self.__intercambio


# Función principal o main
if __name__ == '__main__':
    try:
        numero = int(input("Ingrese un numero de 4 digitos: "))
        # Ver que el usuario cumpla con la condicion de 4 digitos
        if 10000 > numero > 999:
            Algoritmo = Algoritmo(numero)
            Algoritmo.suma(numero)
            print("Su nuevo numero es: ", Algoritmo.get_intercambio())
        else:
            print("Tiene que ingresar un numero de 4 digitos")

    except ValueError:
        print("No ingreso un numero")
