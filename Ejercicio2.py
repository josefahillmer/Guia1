#!/usr/bin/python
# -*- coding: utf-8 -*-


# Clase que representa a un libro
class Libro():

    # Constructor de la clase libro
    def __init__(self, titulo, autor):
        # Objeto privado
        self.__titulo = titulo
        self.__autor = autor

    def get_titulo(self):
        return self.__titulo

    def get_autor(self):
        return self.__autor

    def set_titulo(self, titulo):
        self.__titulo = titulo

    def set_autor(self, autor):
        self.__autor = autor


# Clase que representa una biblioteca
class Biblioteca():

    # Constructor de la clase biblioteca
    def __init__(self, total, prestados):
        self.__total = total
        self.__prestados = prestados

    def get_total(self):
        return self.__total

    def get_prestados(self):
        return self.__prestados

    def set_total(self, total):
        self.__total = total

    def set_prestados(self, prestados):
        return self.__prestado


# Función principal o main
if __name__ == '__main__':
    Libro1 = Libro("Python para todos", "Raúl Gonzáles")
    Libroa = Biblioteca(7, 2)
    print("Libro 1:")
    print("Titulo del libro: ", Libro1.get_titulo())
    print("Autor del libro: ", Libro1.get_autor())
    print("Total de ejempleares: ", Libroa.get_total())
    print("Total de libros prestados: ", Libroa.get_prestados())

    Libro2 = Libro("Cálculo", "James Stewart")
    Librob = Biblioteca(10, 9)
    print("Libro 2:")
    print("Titulo del libro: ", Libro2.get_titulo())
    print("Autor del libro: ", Libro2.get_autor())
    print("Total de ejempleares: ", Librob.get_total())
    print("Total de libros prestados: ", Librob.get_prestados())
