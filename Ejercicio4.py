#!/usr/bin/python
# -*- coding: utf-8 -*-


# Clase que representa a un Perro
class Amigos():

    def __init__(self, cuenta, disponible):
        self.cuenta = cuenta
        self.__disponible = disponible

    def get_disponible(self):
        return self.__disponible

    def set_disponible(self, disponible, cuenta):
        # El saldo disponible debe ser mayor a la cuenta
        if disponible >= cuenta:
            # Para sacar cuanto es el iva
            precio_iva = cuenta * 0.19
            # Para sacar cuanto es la propina
            precio_prop = cuenta * 0.1
            # Cuenta final con iva y propina incluida
            precio_final = precio_iva + precio_prop + cuenta
            # Se divide entre los tres amigos
            precio = precio_final / 3
            print("El saldo disponible: ", disponible)
            print("El precio del IVA es: $", precio_iva)
            print("El precio de la propina es: $", precio_prop)
            print("Total a pagar: $", precio_final)
            print("Cada amigo debe pagar: $:", precio)
            self.__disponible = disponible
            # Si la cuenta toal es mayor que el saldo, un amigo paga
            if precio_final > disponible:
                diferencia = precio_final - disponible
                print("Juan pagara la diferencia: $", diferencia)
                print("Cada amigo le deba a Juan: $", diferencia / 3)

        else:
            print("El valor de la cuenta es mayor al dinero disponible")


# Función principal o main
if __name__ == '__main__':
    print("Pepe va a un restaurante con Juan y Diego y deben pagar la cuenta")
    try:
        saldo = int(input("Ingrese el saldo de Pepe: $"))
        saldo1 = int(input("Ingrese el saldo del Juan: $"))
        saldo2 = int(input("Ingrese el saldo del Diego: $"))
        cuenta = int(input("Ingrese el valor de la cuenta: $"))
        disponible = saldo + saldo1 + saldo2
        Amigos = Amigos(cuenta, disponible)
        Amigos.set_disponible(disponible, cuenta)

    except ValueError:
        print("Ingrese los saldos en numeros")

